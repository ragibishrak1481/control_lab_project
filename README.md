# Computer Vision Based Prototype Automatic Goalkeeper. 
A webcam from above the field captures the image. 
The image is processed in Matlab to identify the ball, predict trajectory and goalkeeper is placed accordingly to stop the ball.

## Hardware:
    1. Arduino to control the goalkeeper
    2. A 2.5x2.5 feet platform used as the field
    3. A 4 mega pixel webcam
    4. One DC Motor

## Software:
    Matlab

## Algorithm:
    1. Image is captured using the webcam
    2. Two consecutive image is processed to get approximate linear trajectory of the ball
    3. The 2 consecutive positions are interpolated to predict where the ball would cross the goalkeeper.
    4. The goalkeeper is placed at the predicted position using a arduino controlled motor.
    N.B. The position of ball is calculated using hough transform

## Shortcomings:
    If the ball is hit too fast the goalkeeper fails to stop the ball. The main reason is that it uses hough transform which
    is computationally intensive. So, if the ball is hit too fast it only finds one position. Sometimes it can not even see
    the ball because of the very low frame rate of cheap webcam.
    
