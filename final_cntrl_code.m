%% CONTROL PROJECT COMPLETE CODE
% Author: RAGIB ISHRAK
%
% Co-Author: SHOAIB MAHMUD
%
% Co-Co-Author: NAFIZ ABEER
%
%% ========================= Starting =====================================
while(input('ok?')~=1)
clear all;
close all;
clc;

%% ===================== parameter declaration ============================
xmin = 1;
xmax = 640;
ymin = 1;
ymax = 480;
% =========================================================================
%% ==================== arduino initialization ============================
motor1 = 'D11';
motor2 = 'D12';
arduino_port = 'COM3';
arduino_name = 'Uno';
a = arduino('COM3','Uno');
configurePin(a,motor1,'DigitalOutput');
configurePin(a,motor2,'DigitalOutput');
% =========================================================================
%% ===================== data acquisition initialization ==================
vid = videoinput('winvideo', 3,'YUY2_640x480');
set(vid,'FramesPerTrigger',Inf);
set(vid,'ReturnedColorspace','rgb');
vid.FrameGrabInterval=1;
start(vid);
% =========================================================================
%% ===================== move the keeper to the right/left ================
writeDigitalPin(a,motor1,0);
writeDigitalPin(a,motor2,1);
pause(.7)
writeDigitalPin(a,motor1,0);
writeDigitalPin(a,motor2,0);
% =========================================================================
%% ====================== move the keeper to the middle ==================
writeDigitalPin(a,motor1,1);
writeDigitalPin(a,motor2,0);
pause(.25)
writeDigitalPin(a,motor1,0);
writeDigitalPin(a,motor2,0);
% =========================================================================
%% ====================== image averaging for reference image =============
sumimg = double(zeros(480,640,3));
for i=1:30
    sumimg = sumimg + double(getsnapshot(vid));
end
m = max(max(max(sumimg)));
ref = 256*sumimg/m;
pic1 = uint8(ref);
imshow(pic1);    % pic1 is the reference image
% =========================================================================
%% ======================= parameter tuning ===============================
color_threshold_level = 0.25;
filter_radius = 300;
gk_x = 30; % goal keeper initial x position
pt_x = 280; % threshold for breaking condition
post_ybound=[100 350]; % goal post bound;
gk_y = mean(post_ybound);  %goal keeper initial y position
tol1 = 10; %tolerance for center update
tol2 = 10; %tolerance for breaking condition
% =========================================================================
%% ======================= preallocation ==================================
color_imsize = [(ymax-ymin+1) (xmax-xmin+1) 3];
bwimsize = [(ymax-ymin+1) (xmax-xmin+1)];
pic2 = zeros(color_imsize);
i1 = zeros(color_imsize);
i2 = zeros(bwimsize);
i3 = zeros(bwimsize);
ball_center = zeros(2,2);
prev=zeros(1,2);
current=[floor(ymax/2) xmax];
left=0; % motor control
y_pred=0; % predicted position of the ball
change = 0;
% =========================================================================
%% ==================== ready whistle =====================================
amp=25; 
fs=20500; % sampling frequency
duration=.25;
freq=1000;
values=0:1/fs:duration;
qq=amp*sin(2*pi* freq*values);
sound(qq);
%% ========================= center capturing loop
    while(1)
        tic
        
        pic = getsnapshot(vid);
        pic2 = pic(ymin:ymax,xmin:xmax,:);
        i1 = imabsdiff(pic1,pic2);
        i2 = im2bw(i1,color_threshold_level);
        i3 = bwareaopen(i2,filter_radius);
        cc = bwconncomp(i3);
        c = regionprops('table',cc,'centroid');

        if size(c,1) == 0
            continue
        end

        center = c.Centroid
        center_x = center(:,1)';
        distance_x = center_x - gk_x;
        [~,max_index] = max(distance_x);
        prev = current;
            
        %update if difference is greater than tolerance
        tol1=10;
        tol2=10;
        if abs(center_x(max_index)-prev(1))>=tol1
%             j = j+1
            current=center(max_index,:);
        end
        
        % breaking condition
        if( (abs(current(1)-pt_x) < tol2) || ((center(1)-pt_x)<0) )
            toc
            break
        end
        
%         imshow(i3)
%         viscircles(center(:,:),5*ones(1,size(center,1)),'Color','b');

        toc
       
    end
    
    % ==================== prediction of the ball position ================
      y_pred=interp1([prev(1) current(1)],[prev(2) current(2)],gk_x,'spline','extrap');
      %if y_pred is in between the posts send the command
      if y_pred>post_ybound(1) && y_pred<post_ybound(2)
          change = gk_y-y_pred;
          if change <0 
              left = 0;
          elseif change>0
              left = 1;
          else
              % motor will not move;
          end
%       else
%           continue
      end
      
      
    % =====================================================================
    % ==================== move the keeper ================================
    pwm=abs(change/(post_ybound(2)-post_ybound(1)))
    if abs(pwm)>eps
        writeDigitalPin(a,motor1,left);
        writeDigitalPin(a,motor2,1-left);
        pause(pwm*.6);
        writeDigitalPin(a,motor1,0);
        writeDigitalPin(a,motor2,0);
    end
    
    pause(2);

%% 
stop(vid)
flushdata(vid);
end